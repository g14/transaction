Feature: Test MerchantDAO operations for merchants


  Scenario: add transaction
    Given a transaction database
    When adding a transaction
    Then a transaction is added

  Scenario: Get transaction by token
    Given a transaction database
    And a token "38e40d1a-9cd0-4c13-8c09-207b6ede305a"
    When fetching a transaction
    Then the transaction is fetched


  Scenario: Get unknown transaction by token
    Given a transaction database
    And a token "IAmAnUnknownTokenAndIDoesn'tExist"
    When fetching an unknown transaction
    Then an Unknown token exception is thrown


  Scenario: GetAllTransactions
    Given a transaction database
    When fetching all transactions
    Then all transactions are fetched

  Scenario: get all merchant transactions
    Given a transaction database
    And an ID 1
    When fetching all merchant transactions
    Then all merchant transactions are fetched

  Scenario: get all merchant transactions within date
    Given a transaction database
    And an ID 2
    And a from date "2021-01-01 00:00:00"
    And a to date "2021-01-31 00:00:00"
    When fetching all merchant transactions within dates
    Then all merchant transactions within dates are fetched

  Scenario: get all customer transactions
    Given a transaction database
    And an ID 2
    When fetching all customer transactions
    Then all customer transactions are fetched

  Scenario: get all customer transactions within date
    Given a transaction database
    And an ID 1
    And a from date "2021-01-01 00:00:00"
    And a to date "2021-01-31 00:00:00"
    When fetching all customer transactions within dates
    Then all customer transactions within dates are fetched
