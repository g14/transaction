Feature: Send a message
  Scenario: Send the event BankIdQueue when receiving the event CustomerIdQueue
    Given receiving event "GetTransactionByTokenQueue"
    Then send event "GetTransactionQueue"

  Scenario: Add transactions
    Given event "AddTransactionQueue"
    Then event is received
    And transaction is added to the database