package stepDefinition;

import exceptions.UnknownTokenException;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import items.Transaction;
import messaging.Event;
import messaging.EventSender;
import messaging.service.Service;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import transaction.TransactionDAO;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;


public class ServiceStepdefs {
    Service service;
    Event event;

    public ServiceStepdefs () {
        service = new Service();
        service.setSender(new EventSender() {
            @Override
            public void sendEvent(Event e) throws Exception {
                event = e;
            }
        });
    }



    @Before
    public void beforeScenario() {
        File source = new File("src/main/resources/persistantDatabaseFiles/transactions.json");
        File dest = new File("src/main/java/database/databasefile/transactions.json");
        try {
            FileUtils.copyFile(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Given("receiving event {string}")
    public void receivingEvent(String eventName) throws Exception {
        service.receiveEvent(new Event(eventName, new Object[]{"38e40d1a-9cd0-4c13-8c09-207b6ede305a"}));

    }

    @Then("send event {string}")
    public void sendEvent(String arg0) {
        Assertions.assertEquals(arg0, event.getEventType());
    }

    @Then("event is received")
    public void eventIsReceived() {
    }

    @And("transaction is added to the database")
    public void transactionIsAddedToTheDatabase() {
        TransactionDAO transactionDAO = new TransactionDAO();
        try {
            assertNotNull(transactionDAO.getTransactionByToken("uniqueToken"));
        } catch (UnknownTokenException e) {
            e.printStackTrace();
        }
    }

    @Given("event {string}")
    public void event(String arg0) throws Exception {
        Transaction transaction = new Transaction(1, 1, 1, 100, "uniqueToken", "2020-01-13 00:00:00", "Purchase");
        Event event = new Event("AddTransactionQueue", new Object[]{
                ((Integer) transaction.getMerchant_id()).doubleValue(),
                ((Integer) transaction.getCustomer_id()).doubleValue(),
                ((Integer) transaction.getAmount()).doubleValue(),
                transaction.getToken(),
                transaction.getDateAsString(),
                transaction.getType()});
        service.receiveEvent(event);

    }
}
