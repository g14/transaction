package stepDefinition.restAPI;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class RestAPIStepDefs {
    int id;
    String cpr;
    String cvr;
    String bank_id;
    String firstName;
    String lastName;


    @Before
    public void beforeScenario() {
        File source = new File("src/main/resources/persistantDatabaseFiles/transactions.json");
        File dest = new File("src/main/java/database/databasefile/transactions.json");
        try {
            FileUtils.copyFile(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Given("the id {int}")
    public void theId(int id) {
        this.id = id;
    }


}
