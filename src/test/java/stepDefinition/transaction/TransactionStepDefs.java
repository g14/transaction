package stepDefinition.transaction;

import exceptions.UnknownTokenException;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import items.CustomerTransaction;
import items.MerchantTransaction;
import items.Transaction;
import org.apache.commons.io.FileUtils;
import transaction.TransactionDAO;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TransactionStepDefs {

    private TransactionDAO transactionDAO;
    private int id;
    private String token;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private Transaction transaction;
    private UnknownTokenException exception;
    private List<Transaction> transactions;
    private List<MerchantTransaction> merchantTransactions;
    private List<CustomerTransaction> customerTransactions;

    private static final String dateFormat = "yyyy-MM-dd HH:mm:ss";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);


    @Before
    public void beforeScenario() {
        File source = new File("src/main/resources/persistantDatabaseFiles/transactions.json");
        File dest = new File("src/main/java/database/databasefile/transactions.json");
        try {
            FileUtils.copyFile(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Given("a transaction database")
    public void aTransactionDatabase() {
        transactionDAO = new TransactionDAO();
    }

    @And("a token {string}")
    public void aToken(String token) {
        this.token = token;
    }

    @When("fetching a transaction")
    public void fetchingATransaction() {
        try {
            transaction = transactionDAO.getTransactionByToken(token);
        } catch (UnknownTokenException e) {
            fail();
        }
    }

    @Then("the transaction is fetched")
    public void theTransactionIsFetched() {
    }


    @When("fetching an unknown transaction")
    public void fetchingAnUnknownTransaction() {
        try {
            transaction = transactionDAO.getTransactionByToken(token);
        } catch (UnknownTokenException e) {
            this.exception = e;
        }
    }

    @Then("an Unknown token exception is thrown")
    public void anUnknownTokenExceptionIsThrown() {
        assertNull(transaction);
        assertNotNull(exception);
    }

    @When("fetching all transactions")
    public void fetchingAllTransactions() {
        transactions = this.transactionDAO.getTransactions();
    }

    @Then("all transactions are fetched")
    public void allTransactionsAreFetched() {
        List<Transaction> expectedTransactions = new ArrayList<>();
        expectedTransactions.add(new Transaction(1, 1, 1, 100, "58e40d1a-9cd0-4c13-8c09-207b6ede305a", "2020-01-13 00:00:00", "Purchase"));
        expectedTransactions.add(new Transaction(2, 2, 1, 100, "38e40d1a-9cd0-4c13-8c09-207b6ede305a", "2020-01-13 00:00:00", "Purchase"));
        expectedTransactions.add(new Transaction(3, 1, 1, 100, "b6eb6de3-1fb8-481d-ba91-9cc05eef408b", "2020-01-13 00:00:00", "Purchase"));
        expectedTransactions.add(new Transaction(4, 2, 2, 100, "b6eb6de3-1fb8-481d-ba91-9cc05eef408b", "2020-01-13 00:00:00", "Purchase"));
        expectedTransactions.add(new Transaction(5, 2, 2, 100, "b6eb6de3-1fb8-481d-ba91-9cc05eef408b", "2020-01-13 00:00:00", "Purchase"));
        expectedTransactions.add(new Transaction(6, 1, 2, 100, "b6eb6de3-1fb8-481d-ba91-9cc05eef408b", "2021-01-13 00:00:00", "Purchase"));
        expectedTransactions.add(new Transaction(7, 2, 1, 100, "a6eb6de3-1fb8-481d-ba91-9cc05eef408b", "2021-01-13 00:00:00", "Refund"));
        assertEquals(expectedTransactions, transactions);
    }

    @When("fetching all merchant transactions")
    public void fetchingAllMerchantTransactions() {
        this.merchantTransactions = this.transactionDAO.getMerchantTransactions(this.id);
    }

    @Then("all merchant transactions are fetched")
    public void allMerchantTransactionsAreFetched() {
        List<MerchantTransaction> expectedTransactions = new ArrayList<>();
        expectedTransactions.add(new MerchantTransaction(1, 1, 100, "58e40d1a-9cd0-4c13-8c09-207b6ede305a", LocalDateTime.parse("2020-01-13 00:00:00", formatter), "Purchase"));
        expectedTransactions.add(new MerchantTransaction(3, 1, 100, "b6eb6de3-1fb8-481d-ba91-9cc05eef408b", LocalDateTime.parse("2020-01-13 00:00:00", formatter), "Purchase"));
        expectedTransactions.add(new MerchantTransaction(6, 1, 100, "b6eb6de3-1fb8-481d-ba91-9cc05eef408b", LocalDateTime.parse("2021-01-13 00:00:00", formatter), "Purchase"));
        assertEquals(expectedTransactions, merchantTransactions);
    }

    @And("a from date {string}")
    public void aFromDate(String fromDate) {
        this.fromDate = LocalDateTime.parse(fromDate, formatter);
    }

    @And("a to date {string}")
    public void aToDate(String toDate) {
        this.toDate = LocalDateTime.parse(toDate, formatter);
    }

    @Then("all merchant transactions within dates are fetched")
    public void allMerchantTransactionsWithinDatesAreFetched() {
        List<MerchantTransaction> expectedTransactions = new ArrayList<>();
        expectedTransactions.add(new MerchantTransaction(7, 2, 100, "a6eb6de3-1fb8-481d-ba91-9cc05eef408b", LocalDateTime.parse("2021-01-13 00:00:00", formatter), "Refund"));
        assertEquals(expectedTransactions, merchantTransactions);

    }

    @When("fetching all customer transactions")
    public void fetchingAllCustomerTransactions() {
        customerTransactions = this.transactionDAO.getCustomerTransactions(this.id);
    }

    @Then("all customer transactions are fetched")
    public void allCustomerTransactionsAreFetched() {
        List<CustomerTransaction> expectedTransactions = new ArrayList<>();
        expectedTransactions.add(new CustomerTransaction(4, 2, 2, 100, "b6eb6de3-1fb8-481d-ba91-9cc05eef408b", LocalDateTime.parse("2020-01-13 00:00:00", formatter), "Purchase"));
        expectedTransactions.add(new CustomerTransaction(5, 2, 2, 100, "b6eb6de3-1fb8-481d-ba91-9cc05eef408b", LocalDateTime.parse("2020-01-13 00:00:00", formatter), "Purchase"));
        expectedTransactions.add(new CustomerTransaction(6, 1, 2, 100, "b6eb6de3-1fb8-481d-ba91-9cc05eef408b", LocalDateTime.parse("2021-01-13 00:00:00", formatter), "Purchase"));
        assertEquals(expectedTransactions, customerTransactions);
    }

    @Then("all customer transactions within dates are fetched")
    public void allCustomerTransactionsWithinDatesAreFetched() {
        List<CustomerTransaction> expectedTransactions = new ArrayList<>();
        expectedTransactions.add(new CustomerTransaction(7, 2, 1, 100, "a6eb6de3-1fb8-481d-ba91-9cc05eef408b", LocalDateTime.parse("2021-01-13 00:00:00", formatter), "Refund"));
        assertEquals(expectedTransactions, customerTransactions);
    }


    @And("an ID {int}")
    public void anID(int id) {
        this.id = id;
    }

    @When("fetching all merchant transactions within dates")
    public void fetchingAllMerchantTransactionsWithinDates() {
        this.merchantTransactions = this.transactionDAO.getMerchantTransactions(id, fromDate, toDate);

    }

    @When("fetching all customer transactions within dates")
    public void fetchingAllCustomerTransactionsWithinDates() {
        this.customerTransactions = this.transactionDAO.getCustomerTransactions(id, fromDate, toDate);

    }

    @When("adding a transaction")
    public void addingATransaction() {
        transaction = new Transaction(-1, 2, 3, 100, "newToken", "2022-01-13 00:00:00", "Refund");
        this.token = "newToken";
        this.transactionDAO.addTransaction(transaction);

    }

    @Then("a transaction is added")
    public void aTransactionIsAdded() {
        try {
            Transaction newTransaction = this.transactionDAO.getTransactionByToken(token);
            assertEquals(transaction, newTransaction);
        } catch (UnknownTokenException e) {
            fail();
        }
    }
}
