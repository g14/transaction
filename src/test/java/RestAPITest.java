import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

import java.io.File;
import java.io.IOException;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class RestAPITest {

    @BeforeEach
    public void beforeScenario() {
        File source1 = new File("src/main/resources/persistantDatabaseFiles/transactions.json");
        File dest1 = new File("src/main/java/database/databasefile/transactions.json");
        try {
            FileUtils.copyFile(source1, dest1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getTransactions() {
        given().contentType(ContentType.JSON)
                .queryParam("id", 1)
                .when().get("/transactions/getTransactions")
                .then()
                .statusCode(is(Response.Status.OK.getStatusCode()));
    }

    @Test
    public void getMerchantTransactions() {
        given().contentType(ContentType.JSON)
                .queryParam("id", 1)
                .when().get("/transactions/getMerchantTransactions")
                .then()
                .statusCode(is(Response.Status.OK.getStatusCode()));
    }

    @Test
    public void getMerchantTransactionsWithinDates() {
        given().contentType(ContentType.JSON)
                .queryParam("id", 1)
                .queryParam("fromDate", "2020-01-01 00:00:00")
                .queryParam("toDate", "2020-01-31 00:00:00")
                .queryParam("id", 1)
                .when().get("/transactions/getMerchantTransactionsWithinDates")
                .then()
                .statusCode(is(Response.Status.OK.getStatusCode()));
    }

    @Test
    public void getCustomerTransactions() {
        given().contentType(ContentType.JSON)
                .queryParam("id", 1)
                .when().get("/transactions/getCustomerTransactions")
                .then()
                .statusCode(is(Response.Status.OK.getStatusCode()));
    }

    @Test
    public void getCustomerTransactionsWithinDates() {
        given().contentType(ContentType.JSON)
                .queryParam("id", 1)
                .queryParam("fromDate", "2020-01-01 00:00:00")
                .queryParam("toDate", "2020-01-31 00:00:00")
                .when().get("/transactions/getCustomerTransactionsWithinDates")
                .then()
                .statusCode(is(Response.Status.OK.getStatusCode()));
    }


}

