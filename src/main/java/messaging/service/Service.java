package messaging.service;

import items.Transaction;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import transaction.TransactionDAO;

public class Service implements EventReceiver {
    EventSender sender;
    TransactionDAO transactionDAO = new TransactionDAO();
    Transaction transaction;

    @Override
    public void receiveEvent(Event event) throws Exception {

        // event her er ikke topic. Det er selvde beskedens indhold.
        if (event.getEventType().equals("AddTransactionQueue")) {
            int merchant_id = ((Double) event.getArguments()[0]).intValue();
            int customer_id = ((Double) event.getArguments()[1]).intValue();
            int amount = ((Double) event.getArguments()[2]).intValue();
            String token = (String) event.getArguments()[3];
            String date = (String) event.getArguments()[4];
            String type = (String) event.getArguments()[5];
            transactionDAO.addTransaction(new Transaction(-1, merchant_id, customer_id, amount, token, date, type));
        } else if (event.getEventType().equals("GetTransactionByTokenQueue")) {
            transaction = transactionDAO.getTransactionByToken((String) event.getArguments()[0]);
            Event newEvent = new Event("GetTransactionQueue",
                    new Object[]{transaction.getID(),
                            transaction.getMerchant_id(),
                            transaction.getCustomer_id(),
                            transaction.getAmount(),
                            transaction.getToken(),
                            transaction.getDate(),
                            transaction.getType()});
            sender.sendEvent(newEvent);
        }
    }

    public void setSender(EventSender sender) {
        this.sender = sender;
    }
}
