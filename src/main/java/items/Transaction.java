package items;

import com.google.common.base.Objects;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Transaction {

    private int ID;
    private int merchant_id;
    private int customer_id;
    private int amount;
    private String token;
    private LocalDateTime date;
    private String type;

    private static final String dateFormat = "yyyy-MM-dd HH:mm:ss";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);



    public Transaction(int ID, int merchant_id, int customer_id, int amount, String token, LocalDateTime date, String type) {
        this.ID = ID;
        this.merchant_id = merchant_id;
        this.customer_id = customer_id;
        this.amount = amount;
        this.token = token;
        this.date = date;
        this.type = type;
    }

    public Transaction(int ID, int merchant_id, int customer_id, int amount, String token, String date, String type) {
        this.ID = ID;
        this.merchant_id = merchant_id;
        this.customer_id = customer_id;
        this.amount = amount;
        this.token = token;
        this.date = LocalDateTime.parse(date, formatter);
        this.type = type;


    }


    public int getID() {
        return ID;
    }

    public Transaction setID(int ID) {
        this.ID = ID;
        return new Transaction(ID, this.merchant_id, this.customer_id, this.amount, this.token, this.date, this.type);
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getDate() {
        return date;
    }
    public String getDateAsString() {
        return date.format(formatter);
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return merchant_id == that.merchant_id && customer_id == that.customer_id && amount == that.amount && Objects.equal(ID, that.ID) && Objects.equal(token, that.token) && Objects.equal(date, that.date) && Objects.equal(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(ID, merchant_id, customer_id, amount, token, date, type);
    }

    @Override
    public String toString() {
        return "Transactions{" +
                "ID='" + ID + '\'' +
                ", merchant_id=" + merchant_id +
                ", customer_id=" + customer_id +
                ", amount=" + amount +
                ", token='" + token + '\'' +
                ", date=" + date.format(formatter) +
                ", Type='" + type + '\'' +
                '}';
    }
}
