package items;

import com.google.common.base.Objects;

import java.time.LocalDateTime;

public class CustomerTransaction {

    private int ID;
    private int merchant_id;
    private int customer_id;
    private int amount;
    private String token;
    private LocalDateTime date;
    private String Type;


    public CustomerTransaction(int ID, int merchant_id, int customer_id, int amount, String token, LocalDateTime date, String type) {
        this.ID = ID;
        this.merchant_id = merchant_id;
        this.customer_id = customer_id;
        this.amount = amount;
        this.token = token;
        this.date = date;
        Type = type;
    }

    public static CustomerTransaction fromTransaction(Transaction transaction) {
        return new CustomerTransaction(
                transaction.getID(),
                transaction.getMerchant_id(),
                transaction.getCustomer_id(),
                transaction.getAmount(),
                transaction.getToken(),
                transaction.getDate(),
                transaction.getType());
    }



    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerTransaction that = (CustomerTransaction) o;
        return merchant_id == that.merchant_id && customer_id == that.customer_id && amount == that.amount && Objects.equal(ID, that.ID) && Objects.equal(token, that.token) && Objects.equal(date, that.date) && Objects.equal(Type, that.Type);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(ID, merchant_id, customer_id, amount, token, date, Type);
    }

    @Override
    public String toString() {
        return "Transactions{" +
                "ID='" + ID + '\'' +
                ", merchant_id=" + merchant_id +
                ", customer_id=" + customer_id +
                ", amount=" + amount +
                ", token='" + token + '\'' +
                ", date=" + date +
                ", Type='" + Type + '\'' +
                '}';
    }

}
