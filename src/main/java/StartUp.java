import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;
import messaging.service.Service;
import rest.RestAPI;

/**
 * The whole transaction microservice was written by
 * @author Lasse Alexander Jensen and Duran Köse
 */
@QuarkusMain
public class StartUp implements QuarkusApplication{
    public static void main(String[] args) throws Exception {
        Quarkus.run(StartUp.class, args);
    }

    private void startUp() throws Exception {
        EventSender s = new RabbitMqSender();
        Service service = new Service();
        service.setSender(s);
        new RabbitMqListener(service).listen();
    }


    @Override
    public int run(String... args) throws Exception {
        startUp();
        Quarkus.waitForExit();
        return 0;
    }
}