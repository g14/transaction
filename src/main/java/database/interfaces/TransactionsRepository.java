package database.interfaces;

import exceptions.UnknownTokenException;
import items.CustomerTransaction;
import items.MerchantTransaction;
import items.Transaction;

import java.time.LocalDateTime;
import java.util.List;

public interface TransactionsRepository {

    Transaction getTransactionByToken(String token) throws UnknownTokenException;
    List<Transaction> getTransactions();
    List<CustomerTransaction> getCustomerTransactions(int customer_id);
    List<MerchantTransaction> getMerchantTransactions(int merchant_id);
    List<CustomerTransaction> getCustomerTransactions(int customer_id, LocalDateTime fromDate, LocalDateTime toDate);
    List<MerchantTransaction> getMerchantTransactions(int merchant_id, LocalDateTime fromDate, LocalDateTime toDate);

    void addTransaction(Transaction transaction);

}
