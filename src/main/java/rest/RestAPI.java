package rest;


import items.CustomerTransaction;
import items.MerchantTransaction;
import items.Transaction;
import transaction.TransactionDAO;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Produces("application/json")
@Path("/transactions")
public class RestAPI {

    private static final String dateFormat = "yyyy-MM-dd HH:mm:ss";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);


    @GET
    @Path("/getTransactions")
    public Response getTransactions() {
        List<Transaction> transactions = new TransactionDAO().getTransactions();
        return Response.ok().entity(transactions).build();
    }

    @GET
    @Path("/getMerchantTransactions")
    public Response getMerchantTransactions(@QueryParam("id") int id) {
        List<MerchantTransaction> transactions = new TransactionDAO().getMerchantTransactions(id);
        return Response.ok().entity(transactions).build();
    }

    @GET
    @Path("/getMerchantTransactionsWithinDates")
    public Response getMerchantTransactions(@QueryParam("id") int id, @QueryParam("fromDate") String fromDate, @QueryParam("toDate") String toDate) {
        List<MerchantTransaction> transactions = new TransactionDAO().getMerchantTransactions(id, LocalDateTime.parse(fromDate, formatter), LocalDateTime.parse(toDate, formatter));
        return Response.ok().entity(transactions).build();
    }

    @GET
    @Path("/getCustomerTransactions")
    public Response getCustomerTransactions(@QueryParam("id") int id) {
        List<CustomerTransaction> transactions = new TransactionDAO().getCustomerTransactions(id);
        return Response.ok().entity(transactions).build();
    }

    @GET
    @Path("/getCustomerTransactionsWithinDates")
    public Response getCustomerTransactionsWithinDates(@QueryParam("id") int id, @QueryParam("fromDate") String fromDate, @QueryParam("toDate") String toDate) {
        List<CustomerTransaction> transactions = new TransactionDAO().getCustomerTransactions(id, LocalDateTime.parse(fromDate, formatter), LocalDateTime.parse(toDate, formatter));
        return Response.ok().entity(transactions).build();
    }


}