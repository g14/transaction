package transaction;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import database.interfaces.TransactionsRepository;
import exceptions.UnknownTokenException;

import items.CustomerTransaction;
import items.MerchantTransaction;
import items.Transaction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionDAO implements TransactionsRepository {

    public static final String fileLocation = "src/main/java/database/databasefile/transactions.json";
    private final Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
            .create();




    /**
     * Fetches a transaction with the given token.
     *
     * @param token token
     * @return Transaction that matches token
     * @throws UnknownTokenException UnknownTokenException
     */
    @Override
    public Transaction getTransactionByToken(String token) throws UnknownTokenException {
        List<Transaction> transactions = getTransactions();
        for (Transaction transaction : transactions) {
            if (transaction.getToken().equals(token)) {
                return transaction;
            }
        }
        throw new UnknownTokenException();
    }

    /**
     * Fetches all of the transactions.
     *
     * @return List of transactions
     */
    @Override
    public List<Transaction> getTransactions() {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(fileLocation)) {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray transactionList = (JSONArray) obj;

            //Iterate over transaction array

            return (List<Transaction>) transactionList
                    .stream()
                    .map(emp -> parseTransactionObject((JSONObject) emp))
                    .collect(Collectors.toList());

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    /**
     * Fetches all of the customer transactions
     *
     * @param customer_id id of transactions to fetch
     * @return cstomer transactions
     */
    @Override
    public List<CustomerTransaction> getCustomerTransactions(int customer_id) {
        return getTransactions()
                .stream()
                .filter(customerTransaction -> customerTransaction.getCustomer_id() == customer_id)
                .map(CustomerTransaction::fromTransaction)
                .collect(Collectors.toList());
    }

    /**
     * Fetches all of the merchant transactions
     *
     * @param merchant_id id of transactions to fetch
     * @return merchant transactions
     */
    @Override
    public List<MerchantTransaction> getMerchantTransactions(int merchant_id) {
        return getTransactions()
                .stream()
                .map(MerchantTransaction::fromTransaction)
                .filter(merchantTransaction -> merchantTransaction.getMerchant_id() == merchant_id)
                .collect(Collectors.toList());
    }

    /**
     * Fetches all of the customer transactions from 'fromDate' to 'toDate'
     *
     * @param customer_id customerId
     * @param fromDate    fromDate
     * @param toDate      toDate
     * @return customer transactions
     */
    @Override
    public List<CustomerTransaction> getCustomerTransactions(int customer_id, LocalDateTime fromDate, LocalDateTime toDate) {
        return getTransactions()
                .stream()
                .filter(customerTransaction -> customerTransaction.getCustomer_id() == customer_id)
                .filter(transaction -> transaction.getDate().isBefore(toDate) && transaction.getDate().isAfter(fromDate))
                .map(CustomerTransaction::fromTransaction)
                .collect(Collectors.toList());
    }

    /**
     * Fetches all of the customer transactions from 'fromDate' to 'toDate'
     *
     * @param merchant_id merhcant_id
     * @param fromDate    fromDate
     * @param toDate      toDate
     * @return merchant transactions
     */
    @Override
    public List<MerchantTransaction> getMerchantTransactions(int merchant_id, LocalDateTime fromDate, LocalDateTime toDate) {
        return getTransactions()
                .stream()
                .filter(merchantTransaction -> merchantTransaction.getMerchant_id() == merchant_id)
                .filter(transaction -> transaction.getDate().isBefore(toDate) && transaction.getDate().isAfter(fromDate))
                .map(MerchantTransaction::fromTransaction)
                .collect(Collectors.toList());
    }

    /**
     * Add a transaction to the databaseFile
     *
     * @param newTransaction the new transaction to add. The ID doesn't matter as it will get overwritten.
     */
    @Override
    public void addTransaction(Transaction newTransaction) {
        int largestID = -1;
        List<Transaction> transactions = getTransactions();
        for (Transaction transaction : transactions) {
            if (transaction.getID() > largestID) {
                largestID = transaction.getID();

            }
        }
        largestID++;
        transactions.add(newTransaction.setID(largestID));


        String json = gson.toJson(transactions);
        //Write into the file
        try (FileWriter file = new FileWriter(fileLocation)) {
            file.write(json);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    private static Transaction parseTransactionObject(JSONObject transaction) {

        //Fetch information
        int id = ((Long) transaction.get("ID")).intValue();
        int merchant_id = ((Long) transaction.get("merchant_id")).intValue();
        int customer_id = ((Long) transaction.get("customer_id")).intValue();
        int amount = ((Long) transaction.get("amount")).intValue();
        String token = (String) transaction.get("token");
        String date = (String) transaction.get("date");
        String type = (String) transaction.get("type");


        return new Transaction(id, merchant_id, customer_id, amount, token, date, type);

    }


    private static final class LocalDateTimeAdapter extends TypeAdapter<LocalDateTime> {
        private static final String dateFormat = "yyyy-MM-dd HH:mm:ss";
        private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);


        @Override
        public void write(final JsonWriter jsonWriter, final LocalDateTime localDateTime ) throws IOException {
            jsonWriter.value(localDateTime.format(formatter));
        }

        @Override
        public LocalDateTime read( final JsonReader jsonReader ) throws IOException {
            return LocalDateTime.parse(jsonReader.nextString(), formatter);
        }
    }

}
