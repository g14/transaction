FROM adoptopenjdk:11-jre-hotspot
COPY target/lib /usr/src/lib
COPY target/transactions-1.0.0-SNAPSHOT-runner.jar /usr/src/
COPY target/classes/persistantDatabaseFiles/transactions.json /usr/src/src/main/java/database/databasefile/transactions.json
WORKDIR /usr/src/
CMD java -Xmx64m -jar transactions-1.0.0-SNAPSHOT-runner.jar